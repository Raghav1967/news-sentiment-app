import json
from flask import request, Response
from app.models.alchemy_encoder import AlchemyEncoder
from flask import Blueprint
from app.services.news_service import NewsService

new_service = NewsService()
news_api = Blueprint('news_api', __name__)

@news_api.route('/getNewsFromApi', methods=['GET'])
def create_user():

    results = new_service.do_scrape_news_filter()
    result = json.dumps(results, cls=AlchemyEncoder)

    return Response(response=result, status=200, content_type="application/json")


@news_api.route('/getNewsWithCondition', methods=['GET'])
def get_all_news_with_condition():

    condition = request.args.get('condition')
    results = new_service.get_news_with_condition(condition)
    result = json.dumps(results, cls=AlchemyEncoder)

    return Response(response=result, status=200, content_type="application/json")


@news_api.route('/hi', methods=['GET'])
def test():

    return 'bye'


# @news_api.route('/getSavedNewsForUser', methods=['GET'])
# def get_saved_news_for_user():
#
#     user_id = request.args.get('user_id')
#
#     results = new_service.get_save_news_for_user(user_id)
#     result = json.dumps(results, cls=AlchemyEncoder)
#
#     return Response(response=result, status=200, content_type="application/json")
#

FROM python:3.7-buster

RUN apt-get update -y && \
    apt-get upgrade -y

COPY requirements.txt /src/
WORKDIR /src
RUN pip3 install -r requirements.txt

RUN python -m spacy download en_core_web_sm


COPY app /src/app
COPY run.py /src
COPY static /src/static
COPY templates /src/templates
COPY config/ /src/config

ENV FLASK_ENV=development
EXPOSE 5000

# enable waiting for db
ADD compose-wait/wait /wait
RUN chmod +x /wait



CMD python run.py

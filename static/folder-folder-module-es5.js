function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["folder-folder-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppFolderFolderPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{ folder }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-header collapse=\"condense\">\n    <ion-toolbar>\n      <ion-title size=\"large\">{{ folder }}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <div  *ngIf=\"news\"  [hidden]=\"article\" >\n\n  <div *ngFor= 'let article of news'>\n\n    <ion-card  button='true'>\n      <!-- <ion-img *ngIf = \"article.get_image_url()\" [src]=\"article.get_image_url()\"></ion-img> -->\n      <div>    \n        <ion-button  *ngIf='article.get_saved()==0' slot=\"start\" size=\"small\" color='light' (click)=\"saveArticle(article)\"> <ion-icon name=\"add-outline\"></ion-icon></ion-button>\n        <ion-button  *ngIf='article.get_saved()==1' slot=\"start\" size=\"small\" color='light' (click)=\"unSaveArticle(article)\"> <ion-icon name=\"checkmark-circle\"></ion-icon></ion-button>\n      <div (click) = 'getEntireArtice(article)'>\n        <ion-card-header>\n          <ion-card-subtitle>  {{article.get_sentiment()}}   {{article.get_published_at()}} \n          </ion-card-subtitle>\n          <ion-card-title>  {{article.get_title()}}      </ion-card-title>\n        </ion-card-header>\n      \n        <ion-card-content>\n          <strong>Positive</strong> : {{article.get_logits()[0].toFixed(2)}}\n          <strong>Negative</strong> : {{article.get_logits()[1].toFixed(2)}}\n          <strong>Neutral</strong> : {{article.get_logits()[2].toFixed(2)}}\n\n        </ion-card-content>\n      </div>\n    </div>\n\n    </ion-card>\n</div>\n</div>\n\n<div *ngIf=\"article\" >\n  <ion-card >\n    <ion-item>\n      <ion-button color='light' (click)=\"reset()\"> <ion-icon name=\"arrow-back-outline\"></ion-icon>back</ion-button>\n    </ion-item>\n\n    <!-- <ion-img [src]=\"article.get_image_url()\"></ion-img> -->\n    <ion-card-header>\n      <ion-card-subtitle>  {{article.get_sentiment()}}      </ion-card-subtitle>\n      <ion-card-title>  {{article.get_title()}}      </ion-card-title>\n    </ion-card-header>\n  \n    <ion-card-content *ngIf=\"article.get_text()\">   \n     {{article.get_text()}}\n    </ion-card-content>\n    <!-- <ng-template #content> -->\n      <!-- <ion-card-content>    -->\n          <!-- {{article.get_description()}} -->\n     <!-- </ion-card-content> -->\n    <!-- </ng-template> -->\n\n  </ion-card>\n\n</div>\n\n\n<div *ngIf=\"users\" >\n\n  <div *ngFor= 'let user of users'>\n\n    {{user.username}}\n\n  </div>\n\n</div>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/folder/folder-routing.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/folder/folder-routing.module.ts ***!
    \*************************************************/

  /*! exports provided: FolderPageRoutingModule */

  /***/
  function srcAppFolderFolderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPageRoutingModule", function () {
      return FolderPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _folder_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./folder.page */
    "./src/app/folder/folder.page.ts");

    var routes = [{
      path: '',
      component: _folder_page__WEBPACK_IMPORTED_MODULE_3__["FolderPage"]
    }];

    var FolderPageRoutingModule = function FolderPageRoutingModule() {
      _classCallCheck(this, FolderPageRoutingModule);
    };

    FolderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FolderPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/folder/folder.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/folder/folder.module.ts ***!
    \*****************************************/

  /*! exports provided: FolderPageModule */

  /***/
  function srcAppFolderFolderModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPageModule", function () {
      return FolderPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./folder-routing.module */
    "./src/app/folder/folder-routing.module.ts");
    /* harmony import */


    var _folder_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./folder.page */
    "./src/app/folder/folder.page.ts");

    var FolderPageModule = function FolderPageModule() {
      _classCallCheck(this, FolderPageModule);
    };

    FolderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _folder_routing_module__WEBPACK_IMPORTED_MODULE_5__["FolderPageRoutingModule"]],
      declarations: [_folder_page__WEBPACK_IMPORTED_MODULE_6__["FolderPage"]]
    })], FolderPageModule);
    /***/
  },

  /***/
  "./src/app/folder/folder.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/folder/folder.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppFolderFolderPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-menu-button {\n  color: var(--ion-color-primary);\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3JhZ2hhdi9EZXNrdG9wL2lvbmljL2lvbmljLW5ld3MtbWVudS9zcmMvYXBwL2ZvbGRlci9mb2xkZXIucGFnZS5zY3NzIiwic3JjL2FwcC9mb2xkZXIvZm9sZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLCtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUNDRjs7QURFQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ0NGOztBREVBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7QUNDRjs7QURFQTtFQUNFLHFCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9mb2xkZXIvZm9sZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1tZW51LWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgY29sb3I6ICM4YzhjOGM7XG4gIG1hcmdpbjogMDtcbn1cblxuI2NvbnRhaW5lciBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufSIsImlvbi1tZW51LWJ1dHRvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgY29sb3I6ICM4YzhjOGM7XG4gIG1hcmdpbjogMDtcbn1cblxuI2NvbnRhaW5lciBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/folder/folder.page.ts":
  /*!***************************************!*\
    !*** ./src/app/folder/folder.page.ts ***!
    \***************************************/

  /*! exports provided: FolderPage */

  /***/
  function srcAppFolderFolderPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FolderPage", function () {
      return FolderPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _service_news_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../service/news.service */
    "./src/app/service/news.service.ts");
    /* harmony import */


    var _service_news__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../service/news */
    "./src/app/service/news.ts");

    var FolderPage = /*#__PURE__*/function () {
      function FolderPage(activatedRoute, newsService) {
        _classCallCheck(this, FolderPage);

        this.activatedRoute = activatedRoute;
        this.newsService = newsService;
        this.text = 'HI';
        this.news = [];
        this.saved = 0;
      }

      _createClass(FolderPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.news = [];
          this.folder = this.activatedRoute.snapshot.paramMap.get('id');

          if (this.folder == 'Breaking') {
            this.sentimentSelected = 'Breaking';
          } else if (this.folder == 'Positive') {
            this.sentimentSelected = 'Positive';
          } else if (this.folder == 'Negative') {
            this.sentimentSelected = 'Negative';
          } else if (this.folder == 'Neutral') {
            this.sentimentSelected = 'Neutral';
          } else if (this.folder == 'Saved') {
            this.getSavedNews();
          } // this.getSelectedNews()


          this.getAllUsers();
        }
      }, {
        key: "getEntireArtice",
        value: function getEntireArtice(article) {
          this.article = article;
        }
      }, {
        key: "getAllUsers",
        value: function getAllUsers() {
          var _this = this;

          this.newsService.getAllUsers().subscribe(function (users) {
            console.log(users);
            _this.users = users;
          });
        }
      }, {
        key: "getSelectedNews",
        value: function getSelectedNews() {
          var _this2 = this;

          this.newsService.getSelectedNews(this.sentimentSelected).subscribe(function (neutrals) {
            neutrals.forEach(function (element) {
              if (element['logits'][0]) {
                var article = new _service_news__WEBPACK_IMPORTED_MODULE_4__["News"](element['id'], element['news_filter_id'], element['source'], element['title'], element['content'], element['description'], element['url'], element['image_url'], element['published_at'], element['text'], element['logits'][0].split(',').map(function (x) {
                  return +x;
                }), element['sentiment'], element['saved']);

                _this2.news.push(article);
              }
            });
          });
          console.log(this.news);
        }
      }, {
        key: "getSavedNews",
        value: function getSavedNews() {
          var _this3 = this;

          this.newsService.getSavedNews().subscribe(function (neutrals) {
            neutrals.forEach(function (element) {
              if (element['logits'][0]) {
                var article = new _service_news__WEBPACK_IMPORTED_MODULE_4__["News"](element['id'], element['news_filter_id'], element['source'], element['title'], element['content'], element['description'], element['url'], element['image_url'], element['published_at'], element['text'], element['logits'][0].split(',').map(function (x) {
                  return +x;
                }), element['sentiment'], element['saved']);

                _this3.news.push(article);
              }
            });
          });
          console.log(this.news);
        }
      }, {
        key: "reset",
        value: function reset() {
          this.article = null;
        }
      }, {
        key: "saveArticle",
        value: function saveArticle(article) {
          var _this4 = this;

          article.set_saved(1);
          this.newsService.updateSavedNews(article.get_id(), article.get_saved()).subscribe(function (element) {
            if (element['saved'] == true) {
              _this4.ngOnInit();
            }
          });
        }
      }, {
        key: "unSaveArticle",
        value: function unSaveArticle(article) {
          var _this5 = this;

          article.set_saved(0);
          this.newsService.updateSavedNews(article.get_id(), article.get_saved()).subscribe(function (element) {
            if (element['saved'] == true) {
              _this5.ngOnInit();
            }
          });
        }
      }]);

      return FolderPage;
    }();

    FolderPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _service_news_service__WEBPACK_IMPORTED_MODULE_3__["NewsService"]
      }];
    };

    FolderPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-folder',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./folder.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/folder/folder.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./folder.page.scss */
      "./src/app/folder/folder.page.scss"))["default"]]
    })], FolderPage);
    /***/
  },

  /***/
  "./src/app/service/news.service.ts":
  /*!*****************************************!*\
    !*** ./src/app/service/news.service.ts ***!
    \*****************************************/

  /*! exports provided: NewsService */

  /***/
  function srcAppServiceNewsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsService", function () {
      return NewsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var NewsService = /*#__PURE__*/function () {
      function NewsService(http) {
        _classCallCheck(this, NewsService);

        this.http = http;
        this.Url = 'http://127.0.0.1:5000/'; // URL to web api
      }

      _createClass(NewsService, [{
        key: "getSelectedNews",
        value: function getSelectedNews(condition) {
          return this.http.get(this.Url + 'getNewsWithCondition/?condition=' + condition).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
        }
      }, {
        key: "getSavedNews",
        value: function getSavedNews() {
          return this.http.get(this.Url + 'getSavedNews/').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
        }
      }, {
        key: "updateSavedNews",
        value: function updateSavedNews(id, saved) {
          return this.http.put(this.Url + 'updateSavedArticle', {
            "id": id,
            'saved': saved
          }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
        }
      }, {
        key: "getAllUsers",
        value: function getAllUsers() {
          return this.http.get(this.Url + 'getAllUsers').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError));
        }
        /**
         * Handle Http operation that failed.
         * Let the app continue.
         * @param operation - name of the operation that failed
         * @param result - optional value to return as the observable result
         */

      }, {
        key: "handleError",
        value: function handleError(error) {
          var errorMessage = '';

          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = "Error: ".concat(error.error.message);
          } else {
            // server-side error
            errorMessage = "Error Code: ".concat(error.status, "\nMessage: ").concat(error.message);
          }

          console.log(errorMessage);
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(errorMessage);
        }
      }]);

      return NewsService;
    }();

    NewsService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    NewsService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], NewsService);
    /***/
  },

  /***/
  "./src/app/service/news.ts":
  /*!*********************************!*\
    !*** ./src/app/service/news.ts ***!
    \*********************************/

  /*! exports provided: News */

  /***/
  function srcAppServiceNewsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "News", function () {
      return News;
    });

    var News = /*#__PURE__*/function () {
      function News( // _id, , source, title, content, description, url, image_url, published_at, text,
      // logits, sentiment
      _id, news_filter_id, source, title, content, description, url, imageUrl, publishedAt, text, logits, sentiment, saved) {
        _classCallCheck(this, News);

        this._id = _id;
        this.news_filter_id = news_filter_id;
        this.source = source;
        this.title = title;
        this.content = content;
        this.description = description;
        this.url = url;
        this.imageUrl = imageUrl;
        this.publishedAt = publishedAt;
        this.text = text;
        this.logits = logits;
        this.sentiment = sentiment;
        this.saved = saved;
      }

      _createClass(News, [{
        key: "get_id",
        value: function get_id() {
          return this._id;
        }
      }, {
        key: "get_news_filter_id",
        value: function get_news_filter_id() {
          return this.news_filter_id;
        }
      }, {
        key: "get_source",
        value: function get_source() {
          return this.source;
        }
      }, {
        key: "get_title",
        value: function get_title() {
          return this.title;
        }
      }, {
        key: "get_content",
        value: function get_content() {
          return this.content;
        }
      }, {
        key: "get_description",
        value: function get_description() {
          return this.description;
        }
      }, {
        key: "get_text",
        value: function get_text() {
          return this.text;
        }
      }, {
        key: "get_logits",
        value: function get_logits() {
          return this.logits;
        }
      }, {
        key: "get_sentiment",
        value: function get_sentiment() {
          return this.sentiment;
        }
      }, {
        key: "get_url",
        value: function get_url() {
          return this.url;
        }
      }, {
        key: "get_image_url",
        value: function get_image_url() {
          return this.imageUrl;
        }
      }, {
        key: "get_published_at",
        value: function get_published_at() {
          return this.publishedAt;
        }
      }, {
        key: "get_saved",
        value: function get_saved() {
          return this.saved;
        }
      }, {
        key: "set_saved",
        value: function set_saved(saved) {
          return this.saved = saved;
        }
      }]);

      return News;
    }();
    /***/

  }
}]);
//# sourceMappingURL=folder-folder-module-es5.js.map